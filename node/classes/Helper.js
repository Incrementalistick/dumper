const moment = require('moment');

module.exports.log = (variable) => {
    if (process.env.DEBUG !== 'true') {
        return;
    }
    console.log(moment().format('DD.MM.YYYY HH.mm.SS') + ' - ', variable);
};