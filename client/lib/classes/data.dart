
import 'Copyable.dart';

class Data extends Copyable {
  String id = '';
  String name = '';

  Data({
    this.id = '',
    this.name = '',
  });

  Data.fromJson(Map json) {
    id = json['id'] ?? '';
    name = json['name'].toString();
  }

  @override
  Data copyWith({
    String? id,
    String? name,
  }) =>
      Data(
        id: id ?? this.id,
        name: name ?? this.name,
      );


  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
  };

}