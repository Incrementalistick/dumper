import 'package:dumper_client/classes/data.dart';
import 'package:dumper_client/classes/options.dart' as classes;
import 'package:flutter/material.dart';

class Options extends StatefulWidget {
  const Options({Key? key}) : super(key: key);

  @override
  State<Options> createState() => _OptionsState();
}

class _OptionsState extends State<Options> {

  late classes.Options _options;

  final _urlKey = GlobalKey<FormFieldState>();
  final TextEditingController _urlController =
    TextEditingController(text: '');

  @override
  void initState() {
    super.initState();

    _options = classes.Options.fromStorage();
    if (_options.url == '') {
      _options.url = 'http://dumper.decrement.ru';
    }
    _urlController.text = _options.url;
    if (_options.keys.isEmpty == true) {
      _options.keys.add(Data());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Настройки'),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(16, 24, 16, 0),
              child: TextFormField(
                key: _urlKey,
                controller: _urlController,
                keyboardType: TextInputType.url,
                decoration: const InputDecoration(
                  labelText: 'Url WS сервера'
                ),
                onChanged: (value) {
                  _options.url = value;
                  _urlKey.currentState!.validate();
                },
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Введите Url';
                  }
                  return null;
                },
              ),
            ),

            Container(
              margin: const EdgeInsets.fromLTRB(16, 24, 16, 0),
              padding: const EdgeInsets.only(bottom: 24.0),
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                border: Border.all(
                  color: const Color.fromRGBO(206, 212, 218, 1),
                  width: 1.0,
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _renderKeysList(),
                  Container(
                    margin: const EdgeInsets.fromLTRB(16, 24, 16, 0),
                    width: double.infinity,
                    child: TextButton(
                      onPressed: () => setState(() => _options.keys.add(Data())),
                      child: const Text('Добавить ключ'),
                    ),
                  ),
                ],
              ),
            ),

            Container(
              margin: const EdgeInsets.fromLTRB(16, 24, 16, 0),
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {
                  if (_urlKey.currentState!.validate() == false) {
                    return;
                  }

                  _options.save();

                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Сохранено')),
                  );
                  Navigator.pop(context, true);
                },
                child: const Text('Сохранить'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _renderKeysList() {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      physics: const NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      itemCount: _options.keys.length,
      itemBuilder: (context, index) {
        Data element = _options.keys[index];
        return Container(
          key: UniqueKey(),
          margin: const EdgeInsets.fromLTRB(16, 24, 16, 0),
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(right: 24.0),
                  child: TextFormField(
                    initialValue: element.id,
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                      labelText: 'Ключ',
                    ),
                    onChanged: (value) {
                      element.id = value;
                    },
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(right: 24.0),
                  child: TextFormField(
                    initialValue: element.name,
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                        labelText: 'Описание'
                    ),
                    onChanged: (value) {
                      element.name = value;
                    },
                  ),
                ),
              ),
              IconButton(
                onPressed: () => setState(() {
                  _options.keys.remove(element);
                }),
                icon: const Icon(Icons.delete, color: Colors.redAccent),
              ),
            ],
          ),
        );
      }
    );
  }

}