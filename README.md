## Применение
* В настройках приложения создать контекстный ключ для отладки
* В коде в нужных местах вызвать API дебагера. Например, функция для PHP
```php
/**
 * @param string $key Ключ для клиента
 * @param string|array $vars Что дампим
 * @param string $file В каком файле (лучше использовать __FILE__)
 * @param int $line В какой строке (лучше использовать __LINE__)
 * @param string $url Url сервера
 * @return mixed
 */
function dumper($key, $vars, $file = '', $line = 0, $url = 'https://dumper.decrement.ru/dumping')
{
    $data = ['key' => $key, 'vars' => (array) $vars, 'file' => $file, 'line' => $line];
    $opts = ['http' => ['method' => 'POST', 'header' => 'Content-type: application/x-www-form-urlencoded', 'content' => json_encode($data)]];
    $opts['https'] = $opts['http'];
    $context = stream_context_create($opts);
    $result = file_get_contents($url, false, $context);
    return @json_decode($result, true);
}
// Её вызов для дебага
$result = dumper('my-debug-key', [
    $integer,
    $float,
    $string,
    $arrayInd,
    $array,
], __FILE__, __LINE__);
/*
 * Результат выполнения в переменной $result:
 * [
 *      'success' => true, // Успешность выполнения
 *      'count' => 1, // Количество клиентов, которым было отправлен дамп
 * ]
 * или
 * [
 *      'success' => false, // Успешность выполнения
 *      'error' => 'Нет клиентов для ключа my-debug-key', // Текст ошибки
 * ]
 */
```
Пример подробнее [ТУТ](./php/test.php)
