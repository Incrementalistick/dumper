const { log } = require('./classes/Helper')
const moment = require('moment')
require('dotenv').config()
const createAdapter = require('socket.io-redis')
const AsciiTable = require('ascii-table')
const { RedisClient } = require('redis')
const fs = require('fs')
const url = require('url');

let helloMessage = new AsciiTable('Starting server')
helloMessage
    .addRow('DEVELOPER', 'Inc (https://decrement.ru)')
    .addRow('NODE JS', process.version)
    .addRow('START TIME', moment().format('DD.MM.YYYY HH:mm:ss Z'))
    .addRow('DEBUG', process.env.DEBUG)
    .addRow('WS HOST', process.env.WS_HOST)
    .addRow('WS PATH', process.env.WS_PATH || '/socket.io')
    .addRow('WS PORT', process.env.WS_PORT)
    .addRow('SECURE', process.env.SECURE)
if (process.env.SECURE_KEY_PATH) {
    helloMessage.addRow('SSL KEY PATH', process.env.SECURE_KEY_PATH)
}
if (process.env.SECURE_CERT_PATH) {
    helloMessage.addRow('SSL CERT PATH', process.env.SECURE_CERT_PATH)
}
console.log(helloMessage.toString())

let server
if (process.env.SECURE === 'true') {
    server = require('https').createServer({
        key: fs.readFileSync(process.env.SECURE_KEY_PATH),
        cert: fs.readFileSync(process.env.SECURE_CERT_PATH),
    })
} else {
    server = require('http').createServer()
}
const ioSocketPath = process.env.WS_PATH || '/socket.io'
const io = require('socket.io')(server, {
    path: ioSocketPath,
    cors: {
        origins: '*',
        methods: ['GET', 'POST'],
    },
})

let clients = []
io.on('connection', client => {
    log(`Connected to default channel - ${client.id}`)
    client.on('disconnect', () => {
        log(`Disconnected to default channel - ${client.id}`)
    })
})
const main = io.of('/main')
main.on('connection', client => {
    const clientKey = client.handshake.headers['x-key']
    log(`Connected to main channel - ${client.id}:${clientKey}`)
    client.join(clientKey)
    clients.push({
        key: clientKey,
        client: client,
    })
    client.on('disconnect', () => {
        log(`Disconnect to main channel - ${client.id}:${clientKey}`)
        client.leaveAll()
        const index = clients.findIndex(val => val.client === client)
        if (index !== -1) {
            clients.splice(index, 1)
        }
    })
})

server.addListener('request', async (request, response) => {
    const urlParts = url.parse(request.url, true);

    if (urlParts.pathname.indexOf(ioSocketPath) === 0) {
        return
    }

    let data = { success: false }

    const buffers = []
    for await (const chunk of request) {
        buffers.push(chunk)
    }
    const requestData = Buffer.concat(buffers).toString()

    if (urlParts.pathname === '/dumping') {
        const jsonData = JSON.parse(requestData)
        let keyClients = clients.filter(val => val.key === jsonData.key)
        if (keyClients.length > 0) {
            keyClients.forEach(keyClient => {
                keyClient.client.emit('dumping', {
                    vars: jsonData.vars,
                    file: jsonData.file,
                    line: jsonData.line,
                })
            })
            data.success = true
            data.count = keyClients.length
        } else {
            data.error = `Нет клиентов для ключа ${jsonData.key}`
        }
    }

    const body = JSON.stringify(data)

    response.writeHead(200, {
        'Content-Length': Buffer.byteLength(body),
        'Content-Type': 'application/json',
    })
    response.end(body)
})

server.listen(process.env.WS_PORT, process.env.WS_HOST)
