
import 'package:dumper_client/classes/data.dart';

class Variable {
  String file;
  int line;
  String vars;
  DateTime dateTime;
  Data key;

  Variable({
    required this.file,
    required this.line,
    required this.vars,
    required this.dateTime,
    required this.key,
  });

}