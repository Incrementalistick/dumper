<?php

require './dumper.php';

$integer = 123234;
$float = 444.23;
$string = 'my string';
$arrayInd = [34, 54, 'saefse'];
$array = ['one' => 1, 'two' => 2, 'three' => 3];

class TestObject
{
    public $prop1 = 'asef';
    public $prop2 = 1243;
    public $prop3 = ['se' => 1];
}

$object = new TestObject();

$result = dumper('test1', [
    $integer,
    $float,
    $string,
    $arrayInd,
    $array,
], __FILE__, __LINE__);

var_dump($result);

$result = dumper('test2', [
    $integer,
], __FILE__, __LINE__);

var_dump($result);
