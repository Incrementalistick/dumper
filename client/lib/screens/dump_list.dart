import 'dart:convert';

import 'package:dumper_client/classes/data.dart';
import 'package:dumper_client/classes/options.dart';
import 'package:dumper_client/classes/variable.dart';
import 'package:dumper_client/components/expandable_fab.dart';
import 'package:dumper_client/screens/options.dart' as screens;
import 'package:flutter/material.dart';
import 'package:flutter_highlight/flutter_highlight.dart';
import 'package:flutter_highlight/themes/github.dart';
import 'package:intl/intl.dart';
import 'package:localstorage/localstorage.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class DumpList extends StatefulWidget {
  const DumpList({Key? key}) : super(key: key);

  @override
  State<DumpList> createState() => _DumpListState();
}

class _DumpListState extends State<DumpList> {
  final LocalStorage _storage = LocalStorage('storage');

  ScrollController _scrollController = ScrollController();

  Options _options = Options();

  List<IO.Socket> _sockets = [];

  List<Variable> _list = [];

  bool _autoScrollEnable = true;

  @override
  void initState() {
    super.initState();

    _storage.ready.then((value) {
      _loadOptions();

      _initSockets();
    });

  }

  void _loadOptions() async {
    Map<String, dynamic>? json = _storage.getItem('options');
    if (json == null) {
      return;
    }
    setState(() {
      _options = Options.fromJson(json);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Список дампов'),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () async {
              bool result = await _toSettings();
              if (result == false) {
                return;
              }
              _loadOptions();

              _initSockets();
            },
          ),
        ],
      ),
      body: _renderBody(),
      floatingActionButton: ExpandableFab(
        distance: 100.0,
        children: [
          ActionButton(
            onPressed: () => setState(() => _list = []),
            icon: const Icon(Icons.delete),
            color: Colors.redAccent,
          ),
          ActionButton(
            onPressed: () {
              setState(() {
                _autoScrollEnable = !_autoScrollEnable;
              });

              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text("Автопрокрутка списка ${_autoScrollEnable ? 'включена' : 'выключена'}")),
              );
            },
            icon: Icon(_autoScrollEnable == true ? Icons.auto_awesome_motion : Icons.auto_awesome_motion_outlined),
          ),
        ],
      ),
    );
  }

  Widget _renderBody() {
    if (_options.url == '') {
      return Container(
        margin: const EdgeInsets.all(16.0),
        padding: const EdgeInsets.all(16.0),
        alignment: Alignment.center,
        width: double.infinity,
        child: const Text('Подтвердите Url WS сервера в настройках'),
      );
    }

    if (_list.isEmpty == true) {
      return Container(
        margin: const EdgeInsets.all(16.0),
        padding: const EdgeInsets.all(16.0),
        alignment: Alignment.center,
        width: double.infinity,
        child: const Text('Нет дампов'),
      );
    }

    return Container(
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          controller: _scrollController,
          itemCount: _list.length,
          itemBuilder: (context, index) {
            Variable element = _list[index];
            DateFormat formatter = DateFormat('dd.MM.yyyy HH:mm:ss');
            return Container(
              key: UniqueKey(),
              margin: EdgeInsets.fromLTRB(16, 24, 16, (index + 1) == this._list.length ? 24.0 : 0),
              padding: const EdgeInsets.all(20.0),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(6.0),
                border: Border.all(
                  color: Colors.black12,
                )
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        margin: const EdgeInsets.only(right: 12.0),
                        decoration: BoxDecoration(
                            color: Colors.teal,
                            borderRadius: BorderRadius.circular(6.0),
                        ),
                        child: Text(
                          element.file,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        margin: const EdgeInsets.only(right: 12.0),
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        child: Text(
                          "Строка: ${element.line.toString()}",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        decoration: BoxDecoration(
                          color: Colors.blueGrey,
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        child: Text(
                          element.key.name != '' ? element.key.name : element.key.id,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            decoration: BoxDecoration(
                              color: Colors.indigo,
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                            child: Text(
                              formatter.format(element.dateTime),
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 16.0),
                    width: double.infinity,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(6.0)),
                      child: HighlightView(
                        element.vars,
                        language: 'json',
                        theme: githubTheme,
                        padding: EdgeInsets.all(4.0),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
      ),
    );
  }

  void _initSockets() {

    for (IO.Socket socket in _sockets) {
      socket.disconnect();
    }
    _sockets = [];
    _list = [];

    if (_options.url == '') {
      return;
    }

    for (Data key in _options.keys) {
      if (key.id == '') {
        continue;
      }
      try {

        IO.Socket socket = IO.io("${_options.url}/main", <String, dynamic>{
          'transports': <String>['websocket'],
          'path': '/socket.io',
          'extraHeaders': <String, dynamic>{'x-key': key.id},
          'multiplex': true,
          'forceNew': true,
        });

        socket.on('dumping', (data) {
          setState(() {
            const JsonEncoder encoder = JsonEncoder.withIndent('  ');
            _list.add(Variable(
              dateTime: DateTime.now(),
              file: data['file'],
              line: data['line'],
              vars: encoder.convert(data['vars']),
              key: key,
            ));
          });
          if (this._autoScrollEnable == true) {
            _scrollToBottom();
          }
        });

        _sockets.add(socket);
      } catch (e) {
        print(e.toString());
      }
    }


  }

  Future<bool> _toSettings() async {
    var result = await Navigator.push(context, MaterialPageRoute(builder: (context) => const screens.Options()));
    if (result == null) {
      return false;
    }

    return true;
  }

  void _scrollToBottom() {
    if (_scrollController.hasClients == true) {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 500), curve: Curves.linear);
    }
  }

  @override
  void dispose() {
    super.dispose();
    for (IO.Socket socket in _sockets) {
      socket.dispose();
    }
  }
}