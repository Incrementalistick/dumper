import 'package:localstorage/localstorage.dart';

import 'data.dart';

class Options {
  String url = '';
  late List<Data> keys;

  Options({
    this.url = '',
    List<Data>? keys,
  }) : keys = keys ?? [];

  Options.fromStorage({String storageName = 'storage'}) {
    final LocalStorage storage = LocalStorage(storageName);
    Map<String, dynamic>? json = storage.getItem('options');
    if (json == null) {
      keys = [];
      return;
    }
    Options options = Options.fromJson(json);
    url = options.url;
    keys = options.keys;
  }

  Options.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    if (json.containsKey('keys') == true) {
      List newKeys = json['keys'];
      keys = newKeys.map((row) => Data.fromJson(row)).toList();
    }
  }

  void save({String storageName = 'storage'}) {
    final LocalStorage storage = LocalStorage(storageName);
    storage.setItem('options', toJson());
  }

  Map<String, dynamic> toJson() => {
    'url': url,
    'keys': keys.map((Data data) => data.toJson()).toList(),
  };

}